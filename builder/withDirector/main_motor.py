from motor import SuperMoto, MotoCross
from director import Director

super_moto = SuperMoto()
director = Director()


# build super moto
director.set_builder(super_moto)
motor = director.get_motor()
motor.get_spec()

# build Moto Cross
motocross = MotoCross()

director.set_builder(motocross)
moto_cross = director.get_motor()
moto_cross.get_spec()
