from abc import ABCMeta, abstractmethod


class Builder:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_wheels(self):
        pass

    @abstractmethod
    def get_engine(self):
        pass

    @abstractmethod
    def get_body(self):
        pass

    @abstractmethod
    def get_color(self):
        pass

