from builder import Builder


class Motor:
    def __init__(self):
        self.__wheels = None
        self.__engine = None
        self.__body = None
        self.__color = None

    def set_body(self, body):
        self.__body = body

    def set_wheel(self, wheels):
        self.__wheels = wheels

    def set_engine(self, engine):
        self.__engine = engine

    def set_color(self, color):
        self.__color = color

    def get_spec(self):
        print("Body: {}".format(self.__body.shape))
        print("Engine: {}".format(self.__engine.engine_capacity))
        print("Wheel: {}".format(self.__wheels.type))
        print("Color: ".format(self.__color.color))


class SuperMoto(Builder):

    def get_color(self):
        color = Color()
        color.color = "Red"
        return color

    def get_wheels(self):
        wheels = Wheel()
        wheels.type = "slick"
        return wheels

    def get_engine(self):
        engine = Engine()
        engine.engine_capacity = 600
        return engine

    def get_body(self):
        body = Body()
        body.shape = "super"
        return body


class MotoCross(Builder):

    def get_color(self):
        color = Color()
        color.color = "Blue"
        return color

    def get_wheels(self):
        wheels = Wheel()
        wheels.type = "off road"
        return wheels

    def get_engine(self):
        engine = Engine()
        engine.engine_capacity = 750
        return engine

    def get_body(self):
        body = Body()
        body.shape = "Moto Cross"
        return body


class Wheel:
    def __init__(self):
        pass

    type = None


class Engine:
    def __init__(self):
        pass

    engine_capacity = None


class Body:
    def __init__(self):
        pass

    shape = None


class Color:
    def __init__(self):
        pass

    color = None