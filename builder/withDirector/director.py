from motor import Motor


class Director:
    def __init__(self):
        pass

    __builder = None

    def set_builder(self, builder):
        self.__builder = builder

    def get_motor(self):
        motor = Motor()

        body = self.__builder.get_body()
        motor.set_body(body)

        engine = self.__builder.get_engine()
        motor.set_engine(engine)

        wheels = self.__builder.get_wheels()
        motor.set_wheel(wheels)

        color = self.__builder.get_color()
        motor.set_color(color)

        return motor
