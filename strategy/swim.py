from abc import ABC, abstractmethod


class SwimBehaviour(ABC):

    @abstractmethod
    def swim(self):
        pass


class Swim(SwimBehaviour):
    def swim(self):
        print("I swim good")


class NoSwim(SwimBehaviour):
    def swim(self):
        print("I can't swim, bol bol bol ....")
