from strategy.duck import ModelDuck, RubberDuck, DecoyDuck, MallardDuck
from strategy.fly import FlyRocketPowered, FlyWithWings, FlyNoWay
from strategy.swim import NoSwim, Swim


# Preparing duck
model = ModelDuck()
mallard = MallardDuck()
decoy = DecoyDuck()
rubber = RubberDuck()

print("\n\n--- testing quacking ---")
mallard.perform_quack()
rubber.perform_quack()
decoy.perform_quack()


print("\n\n--- testing model duck ---")
model.perform_fly()
model.set_fly_behavior(FlyRocketPowered())
model.perform_fly()
model.perform_quack()

print("\n\n--- testing mallard duck ---")
mallard.perform_quack()
mallard.perform_fly()
mallard.set_fly_behavior(FlyRocketPowered())
mallard.perform_fly()

print("\n\n--- testing swimming ---")
model.perform_swim()
mallard.perform_swim()
mallard.set_swim_behaviour(NoSwim())
mallard.perform_swim()
decoy.perform_swim()
rubber.perform_swim()
