from abc import ABCMeta, abstractmethod
from strategy.fly import FlyNoWay, FlyWithWings, FlyRocketPowered
from strategy.quack import Quack, MuteQuack, FakeQuack, Squeak
from strategy.swim import Swim, NoSwim


class Duck:
    __metaclass__ = ABCMeta

    def __init__(self, fly_behavior, quack_behavior, swim_behaviour):
        self.quack_behavior = quack_behavior
        self.fly_behavior = fly_behavior
        self.swim_behavior = swim_behaviour

    @abstractmethod
    def display(self):
        pass

    def set_fly_behavior(self, fb):
        self.fly_behavior = fb

    def set_quack_behavior(self, qb):
        self.quack_behavior = qb

    def set_swim_behaviour(self, sb):
        self.swim_behavior = sb

    def perform_fly(self):
        self.fly_behavior.fly()

    def perform_quack(self):
        self.quack_behavior.quack()

    def perform_swim(self):
        self.swim_behavior.swim()


class ModelDuck(Duck):

    def __init__(self,):
        self.flyBehavior = FlyNoWay()
        self.quackBehavior = Quack()
        self.swim_behavior = NoSwim()
        super(ModelDuck, self).__init__(self.flyBehavior, self.quackBehavior, self.swim_behavior)

    def display(self):
        print("I'm a model duck")


class MallardDuck(Duck):

    def __init__(self):
        self.quackBehavior = Quack()
        self.flyBehavior = FlyWithWings()
        self.swim_behavior = Swim()
        super(MallardDuck, self).__init__(fly_behavior=self.flyBehavior,
                                          quack_behavior=self.quackBehavior,
                                          swim_behaviour=self.swim_behavior)

    def display(self):
        print("I'm a real Mallard duck")


class DecoyDuck(Duck):

    def __init__(self,):
        self.quackBehavior = MuteQuack()
        self.flyBehavior = FlyNoWay()
        self.swim_behavior = Swim()
        super(DecoyDuck, self).__init__(fly_behavior=self.flyBehavior,
                                        quack_behavior=self.quackBehavior,
                                        swim_behaviour=self.swim_behavior)

    def display(self):
        print("I'm a duck Decoy")


class RedHeadDuck(Duck):

    def __init__(self,):
        self.quackBehavior = Quack()
        self.flyBehavior = FlyWithWings()
        self.swim_behavior = Swim()
        super(RedHeadDuck, self).__init__(fly_behavior=self.flyBehavior,
                                          quack_behavior=self.quackBehavior,
                                          swim_behaviour=self.swim_behavior)

    def display(self):
        print("I'm a real Red Headed duck")


class RubberDuck(Duck):

    def __init__(self,):
        self.quackBehavior = Squeak()
        self.flyBehavior = FlyNoWay()
        self.swim_behavior = Swim()
        super(RubberDuck, self).__init__(fly_behavior=self.flyBehavior,
                                         quack_behavior=self.quackBehavior,
                                         swim_behaviour=self.swim_behavior)

    def display(self):
        print("I'm a real Red Headed duck")