from combining.observer.QuackObservable import QuackObservable


class Observable(QuackObservable):

    def __init__(self):
        self.observers = None

    def registerObserver(self, observer):
        self.observers.append(observer)

    def notifyObservers(self):
        for o in self.observers:
            o.update(self.duck)
