from combining.observer.GooseAdapter import GooseAdapter
from combining.observer.Goose import Goose
from combining.observer.QuackCounter import QuackCounter
from combining.observer.CountingDuckFactory import CountingDuckFactory
from combining.observer.Flock import Flock
from combining.observer.Quackologist import Quackologist

def simulate_duck(duck):
    duck.quack()


def simulate(duckFactory):
    mallardDuck = duckFactory.createMallardDuck()
    redheadDuck = duckFactory.createRedheadDuck()
    rubberDuck = duckFactory.createRubberDuck()
    gooseDuck = GooseAdapter(Goose)

    print("Duck Simulator: With Goose Adapter")

    flock = Flock()
    flock.add(mallardDuck)
    flock.add(redheadDuck)
    flock.add(gooseDuck)
    flock.add(rubberDuck)

    simulate_duck(flock)

    print("The ducks quacked " + str(QuackCounter.getQuacks()) + " times")

    mallardDuck2 = duckFactory.createMallardDuck()
    redheadDuck2 = duckFactory.createRedheadDuck()
    rubberDuck2 = duckFactory.createRubberDuck()
    mallardDuck3 = duckFactory.createMallardDuck()
    redheadDuck3 = duckFactory.createRedheadDuck()
    rubberDuck3 = duckFactory.createRubberDuck()

    flock_2 = Flock()
    flock_2.add(mallardDuck2)
    flock_2.add(mallardDuck3)
    flock_2.add(redheadDuck2)
    flock_2.add(redheadDuck3)
    flock_2.add(rubberDuck2)
    flock_2.add(rubberDuck3)

    print("\n Flock 2")

    quackologist = Quackologist()
    flock_2.registerObserver(quackologist)

    simulate_duck(flock_2)

duckFactory = CountingDuckFactory()
simulate(duckFactory)
