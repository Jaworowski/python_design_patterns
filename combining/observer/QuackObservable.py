from abc import ABC, abstractmethod


class QuackObservable(ABC):

    @abstractmethod
    def registerObserver(self, observer):
        pass

    @abstractmethod
    def notifyObservers(self):
        pass
