from combining.observer.Observer import Observer


class Quackologist(Observer):

    def update(self, duck):
        print("Quackologist: " + str(duck.duck) + " just quacked.")
