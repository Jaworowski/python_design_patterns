from combining.observer.AbstractDuckFactory import AbstractDuckFactory
from combining.observer.QuackCounter import QuackCounter
from combining.observer.DuckCall import DuckCall
from combining.observer.MallardDuck import MallardDuck
from combining.observer.RedheadDuck import RedheadDuck
from combining.observer.RubberDuck import RubberDuck


class CountingDuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return QuackCounter(MallardDuck())

    def createRedheadDuck(self):
        return QuackCounter(RedheadDuck())

    def createDuckCall(self):
        return QuackCounter(DuckCall())

    def createRubberDuck(self):
        return QuackCounter(RubberDuck())