from combining.observer.AbstractDuckFactory import AbstractDuckFactory
from combining.observer.DuckCall import DuckCall
from combining.observer.MallardDuck import MallardDuck
from combining.observer.RedheadDuck import RedheadDuck
from combining.observer.RubberDuck import RubberDuck


class DuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return MallardDuck()

    def createRedheadDuck(self):
        return RedheadDuck()

    def createDuckCall(self):
        return DuckCall()

    def createRubberDuck(self):
        return RubberDuck()
