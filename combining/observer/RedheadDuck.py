from combining.observer.Quackable import Quackable


class RedheadDuck(Quackable):

    def quack(self):
        print("Quack")

    def __str__(self):
        return "RedheadDuck"
