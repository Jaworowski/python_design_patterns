from combining.observer.Quackable import Quackable


class DuckCall(Quackable):

    def quack(self):
        print("Kwak")

    def __str__(self):
        return "DuckCall"
