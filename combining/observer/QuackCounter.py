from combining.observer.Quackable import Quackable


class QuackCounter(Quackable):

    numberOfQuacks = 0

    def __init__(self, duck):
        self.duck = duck

    def quack(self):
        self.duck.quack()
        QuackCounter.numberOfQuacks += 1

    @staticmethod
    def getQuacks():
        return QuackCounter.numberOfQuacks
