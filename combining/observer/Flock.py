from combining.observer.Quackable import Quackable
from combining.observer.Observable import Observable


class Flock(Observable):

    def __init__(self,):
        super().__init__()
        self.quackers = []
        self.observers = []

    def add(self, quacker):
        self.quackers.append(quacker)

    def quack(self):
        for q in self.quackers:
            q.quack()
            for o in self.observers:
                o.update(q)

    def registerObserver(self, observer):
        self.observers.append(observer)

