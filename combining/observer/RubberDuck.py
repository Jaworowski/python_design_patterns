from combining.observer.Quackable import Quackable


class RubberDuck(Quackable):

    def quack(self):
        print("Squeak")

    def __str__(self):
        return "RubberDuck"
