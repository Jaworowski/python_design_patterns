from combining.observer.Quackable import Quackable


class MallardDuck(Quackable):

    def quack(self):
        print("Quack")

    def __str__(self):
        return "MallardDuck"
