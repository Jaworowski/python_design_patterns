from abc import ABC, abstractmethod


class AbstractDuckFactory(ABC):

    @abstractmethod
    def createMallardDuck(self):
        pass

    @abstractmethod
    def createRedheadDuck(self):
        pass

    @abstractmethod
    def createDuckCall(self):
        pass

    @abstractmethod
    def createRubberDuck(self):
        pass
