from combining.composite.AbstractDuckFactory import AbstractDuckFactory
from combining.composite.DuckCall import DuckCall
from combining.composite.MallardDuck import MallardDuck
from combining.composite.RedheadDuck import RedheadDuck
from combining.composite.RubberDuck import RubberDuck


class DuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return MallardDuck()

    def createRedheadDuck(self):
        return RedheadDuck()

    def createDuckCall(self):
        return DuckCall()

    def createRubberDuck(self):
        return RubberDuck()
