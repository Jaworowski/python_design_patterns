from combining.composite.AbstractDuckFactory import AbstractDuckFactory
from combining.composite.QuackCounter import QuackCounter
from combining.composite.DuckCall import DuckCall
from combining.composite.MallardDuck import MallardDuck
from combining.composite.RedheadDuck import RedheadDuck
from combining.composite.RubberDuck import RubberDuck


class CountingDuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return QuackCounter(MallardDuck())

    def createRedheadDuck(self):
        return QuackCounter(RedheadDuck())

    def createDuckCall(self):
        return QuackCounter(DuckCall())

    def createRubberDuck(self):
        return QuackCounter(RubberDuck())