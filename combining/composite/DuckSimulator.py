from combining.composite.GooseAdapter import GooseAdapter
from combining.composite.Goose import Goose
from combining.composite.QuackCounter import QuackCounter
from combining.composite.CountingDuckFactory import CountingDuckFactory
from combining.composite.Flock import Flock

def simulate_duck(duck):
    duck.quack()


def simulate(duckFactory):
    mallardDuck = duckFactory.createMallardDuck()
    redheadDuck = duckFactory.createRedheadDuck()
    rubberDuck = duckFactory.createRubberDuck()
    gooseDuck = GooseAdapter(Goose)

    print("Duck Simulator: With Goose Adapter")

    flock = Flock()
    flock.add(mallardDuck)
    flock.add(redheadDuck)
    flock.add(gooseDuck)
    flock.add(rubberDuck)

    simulate_duck(flock)

    print("The ducks quacked " + str(QuackCounter.getQuacks()) + " times")

    mallardDuck2 = duckFactory.createMallardDuck()
    redheadDuck2 = duckFactory.createRedheadDuck()
    rubberDuck2 = duckFactory.createRubberDuck()
    gooseDuck2 = GooseAdapter(Goose)
    mallardDuck3 = duckFactory.createMallardDuck()
    redheadDuck3 = duckFactory.createRedheadDuck()
    rubberDuck3 = duckFactory.createRubberDuck()
    gooseDuck3 = GooseAdapter(Goose)

    flock_2 = Flock()
    flock.add(mallardDuck2)
    flock.add(mallardDuck3)
    flock.add(redheadDuck2)
    flock.add(redheadDuck3)
    flock.add(rubberDuck2)
    flock.add(rubberDuck3)
    flock.add(gooseDuck2)
    flock.add(gooseDuck3)

    print("\n Flock 2")

    simulate_duck(flock)

duckFactory = CountingDuckFactory()
simulate(duckFactory)
