from combining.composite.Quackable import Quackable


class RedheadDuck(Quackable):

    def quack(self):
        print("Quack")
