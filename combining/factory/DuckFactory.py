from combining.factory.AbstractDuckFactory import AbstractDuckFactory
from combining.factory.DuckCall import DuckCall
from combining.factory.MallardDuck import MallardDuck
from combining.factory.RedheadDuck import RedheadDuck
from combining.factory.RubberDuck import RubberDuck


class DuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return MallardDuck()

    def createRedheadDuck(self):
        return RedheadDuck()

    def createDuckCall(self):
        return DuckCall()

    def createRubberDuck(self):
        return RubberDuck()
