from combining.factory.Quackable import Quackable


class DuckCall(Quackable):

    def quack(self):
        print("Kwak")
