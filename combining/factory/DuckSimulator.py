from combining.factory.DuckCall import DuckCall
from combining.factory.MallardDuck import MallardDuck
from combining.factory.RedheadDuck import RedheadDuck
from combining.factory.RubberDuck import RubberDuck
from combining.factory.GooseAdapter import GooseAdapter
from combining.factory.Goose import Goose
from combining.factory.QuackCounter import QuackCounter
from combining.factory.CountingDuckFactory import CountingDuckFactory


def simulate_duck(duck):
    duck.quack()


def simulate(duckFactory):
    mallardDuck = duckFactory.createMallardDuck()
    redheadDuck = duckFactory.createRedheadDuck()
    duckCall = duckFactory.createDuckCall()
    rubberDuck = duckFactory.createRubberDuck()
    gooseDuck = GooseAdapter(Goose)

    print("Duck Simulator: With Goose Adapter")

    simulate_duck(mallardDuck)
    simulate_duck(redheadDuck)
    simulate_duck(duckCall)
    simulate_duck(rubberDuck)
    simulate_duck(gooseDuck)

    print("The ducks quacked " + str(QuackCounter.getQuacks()) + " times")


duckFactory = CountingDuckFactory()
simulate(duckFactory)
