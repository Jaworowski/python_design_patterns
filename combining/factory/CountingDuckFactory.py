from combining.factory.AbstractDuckFactory import AbstractDuckFactory
from combining.factory.QuackCounter import QuackCounter
from combining.factory.DuckCall import DuckCall
from combining.factory.MallardDuck import MallardDuck
from combining.factory.RedheadDuck import RedheadDuck
from combining.factory.RubberDuck import RubberDuck


class CountingDuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return QuackCounter(MallardDuck())

    def createRedheadDuck(self):
        return QuackCounter(RedheadDuck())

    def createDuckCall(self):
        return QuackCounter(DuckCall())

    def createRubberDuck(self):
        return QuackCounter(RubberDuck())