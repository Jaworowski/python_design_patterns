from combining.adapter.Quackable import Quackable


class MallardDuck(Quackable):

    def quack(self):
        print("Quack")
