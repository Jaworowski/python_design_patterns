from combining.adapter.DuckCall import DuckCall
from combining.adapter.MallardDuck import MallardDuck
from combining.adapter.RedheadDuck import RedheadDuck
from combining.adapter.RubberDuck import RubberDuck
from combining.adapter.GooseAdapter import GooseAdapter
from combining.adapter.Goose import Goose


def simulate_duck(duck):
    duck.quack()


def simulate():
    mallardDuck = MallardDuck()
    redheadDuck = RedheadDuck()
    duckCall = DuckCall()
    rubberDuck = RubberDuck()
    gooseDuck = GooseAdapter(Goose)

    print("Duck Simulator: With Goose Adapter")

    simulate_duck(mallardDuck)
    simulate_duck(redheadDuck)
    simulate_duck(duckCall)
    simulate_duck(rubberDuck)
    simulate_duck(gooseDuck)


simulate()