from combining.adapter.Quackable import Quackable


class DuckCall(Quackable):

    def quack(self):
        print("Kwak")
