from combining.decorator.Quackable import Quackable


class RubberDuck(Quackable):

    def quack(self):
        print("Squeak")
