from combining.decorator.Quackable import Quackable


class RedheadDuck(Quackable):

    def quack(self):
        print("Quack")
