from combining.decorator.DuckCall import DuckCall
from combining.decorator.MallardDuck import MallardDuck
from combining.decorator.RedheadDuck import RedheadDuck
from combining.decorator.RubberDuck import RubberDuck
from combining.decorator.GooseAdapter import GooseAdapter
from combining.decorator.Goose import Goose
from combining.decorator.QuackCounter import QuackCounter


def simulate_duck(duck):
    duck.quack()


def simulate():
    mallardDuck = QuackCounter(MallardDuck())
    redheadDuck = QuackCounter(RedheadDuck())
    duckCall = QuackCounter(DuckCall())
    rubberDuck = QuackCounter(RubberDuck())
    gooseDuck = GooseAdapter(Goose)

    print("Duck Simulator: With Goose Adapter")

    simulate_duck(mallardDuck)
    simulate_duck(redheadDuck)
    simulate_duck(duckCall)
    simulate_duck(rubberDuck)
    simulate_duck(gooseDuck)

    print("The ducks quacked " + str(QuackCounter.getQuacks()) + " times")


simulate()