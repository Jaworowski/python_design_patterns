from abc import ABC, abstractmethod


class BeatObserver(ABC):

    @abstractmethod
    def updateBeat(self):
        pass
