from my_design_patterns.combined.djview.HeartModelInterface import HeartModelInterface


class HeartModel(HeartModelInterface):
    def getHeartRate(self):
        pass

    def registerBeatObserver(self, o):
        pass

    def removeBeatObserver(self, o):
        pass

    def registerBPMObserver(self, o):
        pass

    def removeBPMObserver(self, o):
        pass