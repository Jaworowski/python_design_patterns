from my_design_patterns.combined.djview.BeatModel import BeatModel
from my_design_patterns.combined.djview.BeatController import BeatController


model = BeatModel()
controller = BeatController(model)
