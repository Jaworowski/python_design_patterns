from abc import ABC, abstractmethod


class BPMObserver(ABC):

    @abstractmethod
    def updateBPM(self):
        pass
