from my_design_patterns.combined.djview.BeatModelInterface import BeatModelInterface


class BeatModel(BeatModelInterface):
    def initialize(self):
        pass

    def on(self):
        pass

    def off(self):
        pass

    def setBPM(self, bpm):
        pass

    def getBPM(self):
        pass

    def registerBeatObserver(self, o):
        pass

    def removeBeatObserver(self, o):
        pass

    def registerBPMObserver(self, o):
        pass

    def removeBPMObserver(self, o):
        pass