from abc import ABC, abstractmethod


class HeartModelInterface(ABC):

    @abstractmethod
    def getHeartRate(self):
        pass

    @abstractmethod
    def registerBeatObserver(self, o):
        pass

    @abstractmethod
    def removeBeatObserver(self, o):
        pass

    @abstractmethod
    def registerBPMObserver(self, o):
        pass

    @abstractmethod
    def removeBPMObserver(self, o):
        pass
