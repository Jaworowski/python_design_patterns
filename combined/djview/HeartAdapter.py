from my_design_patterns.combined.djview.BeatModelInterface import BeatModelInterface


class HeartAdapter(BeatModelInterface):

    def __init__(self, heart):
        self.heart = heart

    def initialize(self):
        pass

    def on(self):
        pass

    def off(self):
        pass

    def setBPM(self, bpm):
        pass

    def getBPM(self):
        return self.heart.getHeartRate()

    def registerBeatObserver(self, o):
        self.heart.registerBeatObserver(o)

    def removeBeatObserver(self, o):
        self.heart.removeBeatObserver(o)

    def registerBPMObserver(self, o):
        self.heart.registerBPMObserver(o)

    def removeBPMObserver(self, o):
        self.heart.removeBPMObserver(o)

