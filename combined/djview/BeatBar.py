import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QProgressBar
from PyQt5.QtCore import QSize


class BeatBar(QMainWindow):

    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):

        self.setMinimumSize(QSize(640, 200))
        self.setWindowTitle("View")

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)

        gridLayout = QGridLayout(self)
        centralWidget.setLayout(gridLayout)

        progBar = QProgressBar(self)
        progBar.resize(640, 100)
        self.show()
