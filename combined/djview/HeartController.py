from my_design_patterns.combined.djview.ControllerInterface import ControllerInterface
from my_design_patterns.combined.djview.DJView import DJView
from my_design_patterns.combined.djview.HeartAdapter import HeartAdapter


class HeartController(ControllerInterface):

    def __init__(self, model):
        self.model = model
        view = DJView(self, HeartAdapter(model))
        view.createView()
        view.createControls()
        view.disableStopMenuItem()
        view.disableStartMenuItem()

    def start(self):
        pass

    def stop(self):
            pass

    def increaseBPM(self):
            pass

    def decreaseBPM(self):
            pass

    def setBPM(self, bpm):
            pass
