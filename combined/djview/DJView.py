from my_design_patterns.combined.djview.BeatObserver import BeatObserver
from my_design_patterns.combined.djview.BPMObserver import BPMObserver


class DJView(BPMObserver, BeatObserver):

    def __init__(self, controller, model):
        self.controller = controller
        self.model = model
        #TODO nie wiem jak to zrobić
        self.model.registerObserver(BeatObserver)
        self.model.registerObserver(BPMObserver)

    def updateBPM(self):
        pass

    def updateBeat(self):
        pass

    def createView(self):
        pass

    def createControls(self):
        pass

    def enableStopMenuItem(self):
        pass

    def disableStopMenuItem(self):
        pass

    def enableStartMenuItem(self):
        pass

    def disableStartMenuItem(self):
        pass

    def actionPerformed(self):
        pass