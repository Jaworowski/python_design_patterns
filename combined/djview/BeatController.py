from my_design_patterns.combined.djview.ControllerInterface import ControllerInterface
from my_design_patterns.combined.djview.DJView import DJView


class BeatController(ControllerInterface):
    def __init__(self, model):
        self.model = model
        self.view = DJView(self.model)

        self.view.createView()
        self.view.createControls()
        self.view.disableStopMenuItem()
        self.view.enableStartMenuItem()
        self.model.initialize()

    def start(self):
        pass

    def stop(self):
        pass

    def increaseBPM(self):
        pass

    def decreaseBPM(self):
        pass

    def setBPM(self, bpm):
        pass