from command import Command


class Light:

    def on(self):
        print("Light is on")

    def off(self):
        print("Light is off")


class LightOnCommand(Command):

    def __init__(self, light):
        self._light = light

    def execute(self):
        self._light.on()


class LightOffCommand(Command):

    def __init__(self, light):
        self._light = light

    def execute(self):
        self._light.off()