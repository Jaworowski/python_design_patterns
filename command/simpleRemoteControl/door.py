from simpleRemoteControl.command import Command


class GarageDoor:

    def up(self):
        print("Garage door is Open")

    def down(self):
        print("Garage door is Closed")

    def stop(self):
        print("Garage door is Stopped")

    def light_on(self):
        print("Garage light is on")

    def light_off(self):
        print("Garage light is off")


class GarageDoorOpenCommand(Command):

    def __init__(self, garage_door):
        self._garage_door = garage_door

    def execute(self):
        self._garage_door.up()


class GarageLightOnCommand(Command):

    def __init__(self, garage_light):
        self._garage_light = garage_light

    def execute(self):
        self._garage_light.light_on()