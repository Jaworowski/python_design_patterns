from light import Light, LightOffCommand, LightOnCommand
from door import GarageDoor, GarageDoorOpenCommand, GarageLightOnCommand


class SimpleRemoteControl(object):

    def __init__(self):
        self._slot = None

    def set_command(self, command):
        self._slot = command

    def button_was_pressed(self):
        self._slot.execute()


# Remote control test
remote = SimpleRemoteControl()

# initialize objects
light = Light()
garage_door = GarageDoor()

# Prepare command
light_on = LightOnCommand(light)
garage_open = GarageDoorOpenCommand(garage_door)
light_off = LightOffCommand(light)
garage_light_on = GarageLightOnCommand(garage_door)

# set and execute commands
remote.set_command(light_on)
remote.button_was_pressed()

remote.set_command(light_off)
remote.button_was_pressed()

remote.set_command(garage_open)
remote.button_was_pressed()

remote.set_command(garage_light_on)
remote.button_was_pressed()
