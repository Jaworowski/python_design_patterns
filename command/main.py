from abc import ABCMeta, abstractmethod


class Light:

    def on(self):
        print("light ON")

    def off(self):
        print("Light OFF")

class Comamand:
    __metaclass__ = ABCMeta

    @abstractmethod
    def execute(self):
        pass


class SimpleRemoteControl(object):

    def __init__(self):
        self._slot = None

    def set_command(self, command):
        self._slot = command

    def button_pressed(self):
        self._slot.execute()

remote_control = SimpleRemoteControl()
