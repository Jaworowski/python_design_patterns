from light import Light, LightOffCommand, LightOnCommand
from door import GarageDoor, GarageDoorOpenCommand, GarageDoorCloseCommand, GarageLightOnCommand
from cealing_fan import *
from command import NoCommand
from macro import MacroCommand


class RemoteControl(object):

    def __init__(self):
        self._on_commands = [NoCommand()]*7
        self._off_commands = [NoCommand()]*7
        self._undo_command = NoCommand()

    def set_command(self, slot, on_command, off_command):
        self._on_commands[slot] = on_command
        self._off_commands[slot] = off_command

    def button_on_pressed(self, slot):
        self._on_commands[slot].execute()
        self._undo_command = self._on_commands[slot]

    def button_off_pressed(self, slot):
        self._off_commands[slot].execute()
        self._undo_command = self._off_commands[slot]

    def button_undo_pressed(self):
        self._undo_command.undo()

    def remote_description(self):
        print('\n----- Remote Control -----\n')
        for i in range(len(self._on_commands)):
            print("slot " + str(i) + "\t\t" +
                  self._on_commands[i].__class__.__name__ + "\t\t" +
                  self._off_commands[i].__class__.__name__)


# Remote control test
remote = RemoteControl()

# initialize objects
light = Light()
garage_door = GarageDoor()
ceiling_fan = CeilingFan()

# Prepare command
light_on = LightOnCommand(light)
light_off = LightOffCommand(light)

garage_open = GarageDoorOpenCommand(garage_door)
garage_close = GarageDoorCloseCommand(garage_door)
garage_light_on = GarageLightOnCommand(garage_door)

ceiling_fan_high_on = CeilingFanOnCommandHigh(ceiling_fan, "leaving room")
ceiling_fan_medium_on = CeilingFanOnCommandMedium(ceiling_fan, "leaving room")
ceiling_fan_low_on = CeilingFanOnCommandLow(ceiling_fan, "leaving room")
ceiling_fan_off = CeilingFanOffCommand(ceiling_fan, "leaving room")

party_on = MacroCommand([garage_open, ceiling_fan_high_on, light_on])
party_off = MacroCommand([garage_close, ceiling_fan_off, light_off])

# set and execute commands
remote.set_command(slot=0, on_command=light_on, off_command=light_off)
print(remote.remote_description())
remote.button_on_pressed(slot=0)
remote.button_undo_pressed()
remote.button_on_pressed(slot=0)
remote.button_off_pressed(slot=0)

remote.set_command(slot=1, on_command=garage_open, off_command=garage_close)
print(remote.remote_description())
remote.button_on_pressed(slot=1)
remote.button_undo_pressed()
remote.button_on_pressed(slot=1)
remote.button_off_pressed(slot=1)


remote.set_command(slot=2, on_command=ceiling_fan_high_on, off_command=ceiling_fan_off)
remote.set_command(slot=3, on_command=ceiling_fan_medium_on, off_command=ceiling_fan_off)
remote.set_command(slot=4, on_command=ceiling_fan_low_on, off_command=ceiling_fan_off)

print(remote.remote_description())

remote.button_on_pressed(slot=2)
remote.button_on_pressed(slot=3)
remote.button_undo_pressed()
remote.button_off_pressed(slot=4)
remote.button_off_pressed(slot=2)


remote.set_command(slot=5, on_command=party_on, off_command=party_off)

print(remote.remote_description())
remote.button_on_pressed(slot=5)
remote.button_off_pressed(slot=5)
remote.button_undo_pressed()