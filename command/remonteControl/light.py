from command import Command


class Light:

    def __init__(self):
        pass

    def on(self):
        print("Light is on")

    def off(self):
        print("Light is off")


class LightOnCommand(Command):

    def __init__(self, light):
        self._light = light

    def execute(self):
        self._light.on()

    def undo(self):
        self._light.off()


class LightOffCommand(Command):

    def __init__(self, light):
        self._light = light

    def execute(self):
        self._light.off()

    def undo(self):
        self._light.on()
