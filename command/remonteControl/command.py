from abc import ABCMeta, abstractmethod


class Command:
    __metaclass__ = ABCMeta

    @abstractmethod
    def execute(self):
        pass

    @abstractmethod
    def undo(self):
        pass


class NoCommand:

    def __init__(self):
        pass

    def execute(self):
        pass

    def undo(self):
        pass
