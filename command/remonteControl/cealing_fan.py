from command import Command


class CeilingFan:
    HIGH = 3
    MEDIUM = 2
    LOW = 1
    OFF = 0

    def __init__(self):
        self._speed = None
        self._location = None
        self._prev_speed = None

    def off(self):
        self._speed = self.OFF
        print(self._location + " ceiling fan is off")

    def high(self):
        self._prev_speed = self._speed
        self._speed = self.HIGH
        print(self._location + " ceiling fan is on high")

    def medium(self):
        self._prev_speed = self._speed
        self._speed = self.MEDIUM
        print(self._location + " ceiling fan is on medium")

    def low(self):
        self._prev_speed = self._speed
        self._speed = self.LOW
        print(self._location + " ceiling fan is on low")

    def get_speed(self):
        return self._speed

    def set_prev_speed(self, speed):
        self._prev_speed = speed

    def get_prev_speed(self):
        return self._prev_speed

    def set_location(self, location):
        self._location = location


class CeilingFanOnCommandHigh(Command):

    def __init__(self, ceiling_fan, location):
        self._ceiling_fan = ceiling_fan
        self._ceiling_fan.set_location(location)

    def execute(self):
        self._ceiling_fan.set_prev_speed(self._ceiling_fan.get_speed())
        self._ceiling_fan.high()

    def undo(self):
        prev_speed = self._ceiling_fan.get_prev_speed()
        if prev_speed is CeilingFan.HIGH:
            self._ceiling_fan.high()
        elif prev_speed is CeilingFan.MEDIUM:
            self._ceiling_fan.medium()
        elif prev_speed is CeilingFan.LOW:
            self._ceiling_fan.low()
        else:
            self._ceiling_fan.off()


class CeilingFanOnCommandMedium(Command):

    def __init__(self, ceiling_fan, location):
        self._ceiling_fan = ceiling_fan
        self._ceiling_fan.set_location(location)

    def execute(self):
        self._ceiling_fan.set_prev_speed(self._ceiling_fan.get_speed())
        self._ceiling_fan.medium()

    def undo(self):
        prev_speed = self._ceiling_fan.get_prev_speed()
        if prev_speed is CeilingFan.HIGH:
            self._ceiling_fan.high()
        elif prev_speed is CeilingFan.MEDIUM:
            self._ceiling_fan.medium()
        elif prev_speed is CeilingFan.LOW:
            self._ceiling_fan.low()
        else:
            self._ceiling_fan.off()


class CeilingFanOnCommandLow(Command):

    def __init__(self, ceiling_fan, location):
        self._ceiling_fan = ceiling_fan
        self._ceiling_fan.set_location(location)

    def execute(self):
        self._ceiling_fan.set_prev_speed(self._ceiling_fan.get_speed())
        self._ceiling_fan.low()

    def undo(self):
        prev_speed = self._ceiling_fan.get_prev_speed()
        if prev_speed is CeilingFan.HIGH:
            self._ceiling_fan.high()
        elif prev_speed is CeilingFan.MEDIUM:
            self._ceiling_fan.medium()
        elif prev_speed is CeilingFan.LOW:
            self._ceiling_fan.low()
        else:
            self._ceiling_fan.off()


class CeilingFanOffCommand(Command):

    def __init__(self, ceiling_fan, location):
        self._ceiling_fan = ceiling_fan
        self._ceiling_fan.set_location(location)

    def execute(self):
        self._ceiling_fan.set_prev_speed(self._ceiling_fan.get_speed())
        self._ceiling_fan.off()

    def undo(self):
        prev_speed = self._ceiling_fan.get_prev_speed()
        if prev_speed is CeilingFan.HIGH:
            self._ceiling_fan.high()
        elif prev_speed is CeilingFan.MEDIUM:
            self._ceiling_fan.medium()
        elif prev_speed is CeilingFan.LOW:
            self._ceiling_fan.low()
        else:
            self._ceiling_fan.off()
