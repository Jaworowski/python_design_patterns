from template_method.barista.CaffeineBeverageWithHook import CaffeineBeverageWithHook


class Tea(CaffeineBeverageWithHook):

    def brew(self):
        print("Steeping the tea")

    def add_condiments(self):
        print("Adding Lemon and sugar")

    def customer_wants_condiments(self):
        answer = input("Would you like lemon and sugar with your coffee (y/n)?")

        if answer.lower() == 'y':
            return True
        elif answer.lower() == 'n':
            return False
