from template_method.barista.coffee import Coffee
from template_method.barista.tea import Tea


coffee = Coffee()
tea = Tea()

print("\nMaking coffee ...")

coffee.prepare_recipe()

print("\nMaking tea")
tea.prepare_recipe()