from abc import ABCMeta, abstractmethod


class CaffeineBeverageWithHook:
    __metaclass__ = ABCMeta

    def prepare_recipe(self):
        self._boil_water()
        self.brew()
        self._pour_in_cup()
        if self.customer_wants_condiments():
            self.add_condiments()

    def _boil_water(self):
        print("Boiling water")

    @abstractmethod
    def brew(self):
        pass

    @abstractmethod
    def add_condiments(self):
        pass

    def _pour_in_cup(self):
        print("Pouring into cup")

    def customer_wants_condiments(self):
        return True
