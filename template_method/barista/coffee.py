from template_method.barista.CaffeineBeverageWithHook import CaffeineBeverageWithHook


class Coffee(CaffeineBeverageWithHook):

    def brew(self):
        print("Dripping Coffee through filter")

    def add_condiments(self):
        print("Adding Sugar and Milk")

    def customer_wants_condiments(self):
        answer = input("Would you like milk and sugar with your coffee (y/n)?")

        if answer.lower() == 'y':
            return True
        elif answer.lower() == 'n':
            return False
