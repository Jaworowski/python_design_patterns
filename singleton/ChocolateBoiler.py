class ChocolateBoiler(object):
    _instance = None

    @classmethod
    def __init__(cls):
        cls._empty = True
        cls._boiled = False

    def __new__(cls):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            print("Creating new boiler {}".format(cls._instance))
        else:
            print("We have one boiler instance")
        return cls._instance

    def fill(self):
        if self.is_empty():
            print("Filling the boiler")
            self._empty = False
            self._boiled = False

    def drain(self):
        if not self.is_empty() and self.is_boiled():
            print("Boiler is ready to drain")
            self._empty = True

    def boil(self):
        if not self.is_empty() and not self.is_boiled():
            print("Boiling the chocolate")
            self._boiled = True

    def is_empty(self):
        print("Checking boiler fill status")
        return self._empty

    def is_boiled(self):
        print("Checking boiler temperature status")
        return self._boiled

    def get_boiler_state(self):
        print("\nBoiler status:")
        print("Empty is {}".format(self._empty))
        print("Boiled is {}\n".format(self._boiled))
