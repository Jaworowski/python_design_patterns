from singleton.ChocolateBoiler import ChocolateBoiler


boiler = ChocolateBoiler()
boiler.fill()
boiler.get_boiler_state()
boiler2 = ChocolateBoiler()
boiler2.boil()
boiler2.get_boiler_state()
