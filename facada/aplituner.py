class Amplituner:
    def __init__(self, description):
        self._description = description
        self._tuner = None
        self._dvd = None
        self._cd = None

    def on(self):
        print(self._description + " on")

    def off(self):
        print(self._description + " off")

    def set_stereo_sound(self):
        print(self._description + " stereo mode on")

    def set_surround_sound(self):
        print(self._description + " surround sound on (5 speakers, 1 subwoofer)")

    def set_volume(self, level):
        self._description = self._description + " setting volume to " + level

    def set_dvd(self, dvd):
        print(self._description + " setting DVD player to " + " " + str(dvd))
        self._dvd = dvd

    def set_cd(self, cd):
        print(self._description + " setting CD player to " + " " + str(cd))
        self._cd = cd

    def set_tuner(self, tuner):
        print(self._description + " setting CD player to " + " " + str(tuner))
        self._tuner = tuner
