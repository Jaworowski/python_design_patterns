class CdPlayer:
    def __init__(self, description, amp):
        self.amp = amp
        self.title = None
        self.description = description
        self.currentTrack = None

    def on(self):
        print(self.description + " on")

    def off(self):
        print(self.description + " off")

    def eject(self):
        self.title = None
        print(self.description + " eject")

    def play(self, title):
        self.title = title
        self.currentTrack = 0
        print(self.description + " playing '" + title + "'")

    def stop(self):
        self.currentTrack = 0
        print(self.description + " stopped")

    def pause(self):
        print(self.description + " paused '" + self.title + "'")

    def __str__(self):
        return self.description
