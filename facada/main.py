from facada.aplituner import Amplituner
from facada.home_teater_facade import HomeTeatherFacade
from facada.dvd import Dvd
from facada.popcorn_popper import PopcornPopper
from facada.projector import Projector
from facada.screen import Screen
from facada.cd import CdPlayer

amp = Amplituner("Top-O-Line Amplifier")
dvd = Dvd("The Best DvD", "Pepe Pig")
pop = PopcornPopper("Popcorn Popper")
projector = Projector("Top-O-Line Projector", dvd)
screen = Screen("Theater Screen")
cd = CdPlayer("Top-O-Line CD Player", amp)

home_theater = HomeTeatherFacade(amp=amp, dvd=dvd, pop=pop,
                                 projector=projector, screen=screen,
                                 cd=cd)

print("\n\n=====================================")
home_theater.watch_move("Peepe Pig")
home_theater.end_movie()

print("\n\n=====================================")
home_theater.listen_to_cd("Music")
home_theater.end_cd()

