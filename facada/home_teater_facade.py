class HomeTeatherFacade:
    def __init__(self, pop, projector, amp, dvd, screen, cd=None):
        self.pop = pop
        self.amp = amp
        self.dvd = dvd
        self.projector = projector
        self.screen = screen
        self.cd = cd

    def watch_move(self, movie):
        print("Get ready to watch a movie...")
        self.pop.on()
        self.pop.pop()
        self.screen.down()
        self.projector.on()
        self.projector.wide_screen_mode()
        self.amp.on()
        self.amp.set_dvd(dvd=self.dvd)
        self.amp.set_surround_sound()
        self.dvd.on()
        self.dvd.play(movie=movie)

    def end_movie(self):
        print("Shutting movie theater down...")
        self.pop.off()
        self.screen.up()
        self.projector.off()
        self.amp.off()
        self.dvd.stop()
        self.dvd.off()

    def listen_to_cd(self, cdTitle):
        print("Get ready for an audiopile experence...")
        self.amp.on()
        self.amp.set_cd(self.cd)
        self.amp.set_stereo_sound()
        self.cd.on()
        self.cd.play(cdTitle)

    def end_cd(self):
        print("Shutting down CD...")
        self.amp.off()
        self.amp.set_cd(self.cd)
        self.cd.eject()
        self.cd.off()
