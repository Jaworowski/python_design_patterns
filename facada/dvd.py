class Dvd:
    def __init__(self, description, movie):
        self._movie = movie
        self._description = description
        self._current_track = None

    def on(self):
        print(self._description + "on")

    def off(self):
        print(self._description + " off")

    def play(self, movie):
        self._current_track = 0
        print(self._description + " playing '" + movie + "'")

    def stop(self):
        self._current_track = 0
        print(self._description + " stopped '" + self._movie + "' ")

    def pause(self):
        print(self._description + " paused '" + self._movie + "' ")

    def set_two_channel_audio(self):
        print(self._description + " set two channel audio")

    def set_surround_audio(self):
        print(self._description + " set surround audio")
