from adapter.duck import Duck
from wild_turkey import WildTurkey


class TurkeyAdapter(Duck):

    def quack(self):
        self.turkey.gobble()

    def __init__(self, turkey):
        self.turkey = turkey

    def fly(self):
        for i in range(5):
            self.turkey.fly()


class TurkeyClassAdapter(Duck, WildTurkey):
    def quack(self):
        WildTurkey.gobble(self)

    def fly(self):
        for i in range(5):
            WildTurkey.fly(self)
