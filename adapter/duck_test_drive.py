from mallard_duck import MallardDuck
from wild_turkey import WildTurkey
from turkey_adapter import TurkeyAdapter
from turkey_adapter import TurkeyClassAdapter


duck = MallardDuck()
turkey = WildTurkey()
turkey_adapter = TurkeyAdapter(turkey)
turkey_class_adapter = TurkeyClassAdapter()


print("The Turkey says...")
turkey.gobble()
turkey.fly()

print("\nThe Duck says...")
duck.quack()
duck.fly()

print("\nThe Turkey adapter says...")
turkey_adapter.quack()
turkey_adapter.fly()

print("\nThe Turkey classic adapter says...")
turkey_class_adapter.gobble()
turkey_class_adapter.fly()
