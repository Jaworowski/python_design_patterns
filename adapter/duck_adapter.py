from random import randint
from adapter.turkey import Turkey


class DuckAdapter(Turkey):

    def gobble(self):
        self.duck.quack()

    def __init__(self, duck):
        self.duck = duck

    def fly(self):
        if randint(0, 5) == 0:
            self.duck.fly()
