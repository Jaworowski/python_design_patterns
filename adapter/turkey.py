from abc import ABCMeta, abstractmethod


class Turkey:
    __metaclass__ = ABCMeta

    @abstractmethod
    def gobble(self):
        pass

    @abstractmethod
    def fly(self):
        pass
