from mallard_duck import MallardDuck
from duck_adapter import DuckAdapter
from wild_turkey import WildTurkey
from turkey_adapter import TurkeyClassAdapter

turkey = WildTurkey()
duck = MallardDuck()
duck_adapter = DuckAdapter(duck)
turkey_class_adapter = TurkeyClassAdapter()

print("The Turkey says...")
turkey.gobble()
turkey.fly()

print("\nThe Duck says...")
duck.quack()
duck.fly()

print("\nThe DuckAdapter says...")
duck_adapter.gobble()
duck_adapter.fly()

print("\nThe DuckAdapter says...")
turkey_class_adapter.gobble()