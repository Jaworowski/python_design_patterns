from gumball_machine import GumballMachine


gumball = GumballMachine(10)

print(gumball)
print("\n=================")
gumball.insert_quarter()
gumball.turn_crack()
gumball.insert_quarter()
gumball.turn_crack()
print("\n=================")
print(gumball)
gumball.insert_quarter()
gumball.turn_crack()
gumball.insert_quarter()
gumball.turn_crack()
print("\n=================")
print(gumball)

gumball.refill(15)
print(gumball)

print(gumball.get_state())
print(gumball.get_count())

print("\n=================")
gumball.turn_crack()
gumball.eject_quarter()


