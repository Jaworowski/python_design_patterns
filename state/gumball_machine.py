from has_quarter_state import HasQuarterState
from no_quarter_state import NoQuarterState
from sold_state import SoldState
from solid_out_state import SoldOutState
from winner_state import WinnerState


class GumballMachine:

    def __init__(self, number_gumballs):
        self.count = number_gumballs

        self._sold_out_state = SoldOutState(self)
        self._no_quarter_state = NoQuarterState(self)
        self._has_quarter_state = HasQuarterState(self)
        self._sold_state = SoldState(self)
        self._winner_state = WinnerState(self)

        if number_gumballs > 0:
            self._state = self._no_quarter_state
        else:
            self._state = self._sold_out_state

    def insert_quarter(self):
        self._state.insert_quarter()

    def eject_quarter(self):
        self._state.eject_quarter()

    def turn_crack(self):
        self._state.turn_crack()
        self._state.dispense()

    def set_state(self, state):
        self._state = state

    def _release_ball(self):
        print("A gumball comes rolling out the slot...")
        if self.count != 0:
            self.count = self.count - 1

    def get_count(self):
        return self.count

    def refill(self, count):
        self.count = self.count + count
        self._state = self._no_quarter_state

    def get_state(self):
        return self._state

    def get_no_quarter_state(self):
        return self._no_quarter_state

    def get_sold_out_state(self):
        return self._sold_out_state

    def get_has_quarter_state(self):
        return self._has_quarter_state

    def get_sold_state(self):
        return self._sold_state

    def get_winner_state(self):
        return self._winner_state

    def __str__(self):
        if self.count != 1:
            s = "s"
        else:
            s = ""
        return "Mighty Gumball, Inc." \
               "\nJava-enabled Standing Gumball Model #2004" \
               "\nInventory: {} gumball{}" \
               "\nMachine is {}\n".format(self.count, s, self._state)

