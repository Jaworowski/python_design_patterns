from factory.pizzas.pizza import *


class PizzaStore:

    def __init__(self, factory):
        self._factory = factory

    def order_pizza(self, pizza_type):
        pizza = self._factory.create_pizza(pizza_type)

        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()

        return pizza


class SimplePizzaFactory:

    def __init__(self):
        pass

    @staticmethod
    def create_pizza(pizza_type):
        pizza = None

        if pizza_type == "cheese":
            pizza = CheesePizza()
        elif pizza_type == "pepperoni":
            pizza = PepperoniPizza()
        elif pizza_type == "clam":
            pizza = ClamPizza()

        return pizza
