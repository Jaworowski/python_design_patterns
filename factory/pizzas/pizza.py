from abc import ABCMeta, abstractmethod


class Pizza:
    __metaclass__ = ABCMeta

    def __init__(self):
        self._name = None
        if type(self) is Pizza:
            raise NotImplementedError

    def get_name(self):
        return self._name

    def prepare(self):
        print("Preparing " + self._name)

    def bake(self):
        print("Baking " + self._name)

    def cut(self):
        print("Cutting " + self._name)

    def box(self):
        print("Boxing " + self._name)


class CheesePizza(Pizza):

    def __init__(self):
        super(CheesePizza, self).__init__()
        self._name = "Cheese Pizza"
        self._dough = "Regular Crust"
        self._sauce = "Marinara Pizza Sauce"
        self._toppings = ["Fresh Mozzarella", "Parmesan"]


class ClamPizza(Pizza):

    def __init__(self):
        super(ClamPizza, self).__init__()
        self._name = "Clam Pizza"
        self._dough = "Thin crust"
        self._sauce = "White garlic sauce"
        self._toppings = ["Clams", "Grated parmesan cheese"]


class PepperoniPizza(Pizza):

    def __init__(self):
        super(PepperoniPizza, self).__init__()
        self._name = "Pepperoni Pizza"
        self._dough = "Crust"
        self._sauce = "Marinara sauce"
        self._toppings = ["Sliced Pepperoni", "Sliced Onion", "Grated parmesan cheese"]
