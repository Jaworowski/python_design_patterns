from factory.pizzas.stores import SimplePizzaFactory, PizzaStore


factory = SimplePizzaFactory()
store = PizzaStore(factory)

pizza = store.order_pizza("cheese")
print("We ordered a " + pizza.get_name() + "\n")

pizza = store.order_pizza("clam")
print("We ordered a " + pizza.get_name() + "\n")

pizza = store.order_pizza("pepperoni")
print("We ordered a " + pizza.get_name() + "\n")
