class GumballMonitor:
    def __init__(self, machine):
        self._machine = machine

    def report(self):

        print("Gumball Machine: " + self._machine.get_location())
        print("Current inventory: " + str(self._machine.get_count()) + " gumballs")
        print("Current state: " + str(self._machine.get_state()))
