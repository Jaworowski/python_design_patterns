from display import DisplayElement
from observer import Observer


class ForecastDisplay(DisplayElement, Observer):

    def __init__(self, weather_date):
        self._current_pressure = 0
        self._last_pressure = 0
        self._weather_date = weather_date

        weather_date.register_observer(self)

    def display(self):
        print("\n ---- Forcast ---- \n")
        if self._current_pressure > self._last_pressure:
            print("Improving weather on the way!")
        elif self._current_pressure == self._last_pressure:
            print("More of the same")
        else:
            print("Watch out for cooler, rainy weather")

    def update(self, temp, hum, press):
        self._last_pressure = self._current_pressure
        self._current_pressure = press

        self.display()
