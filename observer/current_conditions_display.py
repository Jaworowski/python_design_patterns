from display import DisplayElement
from observer import Observer


class CurrentConditionsDisplay(DisplayElement, Observer):

    def __init__(self, weather_data):
        self._weather_data = weather_data
        self._temperature = None
        self._humidity = None

        weather_data.register_observer(self)

    def update(self, temp, hum, press):
        self._temperature = temp
        self._humidity = hum
        self.display()

    def display(self):
        print("\n ---- Current conditions ---- \n")
        print("{}C degrees and {}% humidity".format(self._temperature, self._humidity))

