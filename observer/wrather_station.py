from observables import Subject


class WeatherStation(Subject):

    def __init__(self):
        self._observers = []
        self._temperature = None
        self._humidity = None
        self._pressure = None

    def notify_observer(self):
        for observer in self._observers:
            observer.update(self._temperature, self._humidity, self._pressure)

    def remove_observer(self, observer):
        self._observers.remove(observer)

    def register_observer(self, observer):
        self._observers.append(observer)

    def get_temperature(self):
        return self._temperature

    def set_measurements(self, temperature, humidity, pressure):
        self._temperature = temperature
        self._humidity = humidity
        self._pressure = pressure

        self.notify_observer()
