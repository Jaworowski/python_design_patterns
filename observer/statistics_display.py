from observer import Observer
from display import DisplayElement


class StatisticsDisplay(Observer, DisplayElement):

    def __init__(self, weather_data):
        self._weather_data = weather_data
        self._max_temp = 0.0
        self._min_temp = 0.0
        self._temp_sume = 0.0
        self._num_readings = 0

        weather_data.register_observer(self)

    def display(self):
        print("\n ---- Statistics ---- \n")
        print("max temp {}C degrees, min temp {}C degrees, average temp {}C"
              .format(self._max_temp, self._min_temp, self._temp_sume / self._num_readings))

    def update(self, temp, hum, press):
        self._num_readings += 1
        self._temp_sume += temp
        if temp < self._min_temp:
            self._min_temp = temp
        if temp > self._max_temp:
            self._max_temp = temp

        self.display()
