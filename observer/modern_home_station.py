from display import DisplayElement
from observer import Observer


class ModerHomeStation(DisplayElement, Observer):

    def __init__(self, weather_data):
        self._weather_data = weather_data
        self._temperature = None
        self._humidity = None
        self._current_pressure = 0
        self._last_pressure = 0
        self._max_temp = 0.0
        self._min_temp = 0.0
        self._temp_sum = 0.0
        self._num_readings = 0

        weather_data.register_observer(self)

    def update(self, temp, hum, press):
        self._temperature = temp
        self._humidity = hum
        self._last_pressure = self._current_pressure
        self._current_pressure = press
        self._num_readings += 1
        self._temp_sum += temp

        if temp < self._min_temp:
            self._min_temp = temp
        if temp > self._max_temp:
            self._max_temp = temp

        self.display()

    def display(self):
        print("\n ---- Modern Home Station ---- \n")
        print("Current conditions:\n "
              "{}C degrees, {}% humidity and pressure {}".format(self._temperature, self._humidity,
                                                                 self._current_pressure))
        print("Forcast")
        if self._current_pressure > self._last_pressure:
            print("Improving weather on the way!")
        elif self._current_pressure == self._last_pressure:
            print("More of the same")
        else:
            print("Watch out for cooler, rainy weather")

        print("statistics: max temp {}C degrees, min temp {}C degrees, average temp {}C"
              .format(self._max_temp, self._min_temp, self._temp_sum / self._num_readings))
