from wrather_station import WeatherStation
from current_conditions_display import CurrentConditionsDisplay
from statistics_display import StatisticsDisplay
from forecast_display import ForecastDisplay
from modern_home_station import ModerHomeStation

weather_station = WeatherStation()

current_display = CurrentConditionsDisplay(weather_data=weather_station)
statistics = StatisticsDisplay(weather_data=weather_station)
forecast = ForecastDisplay(weather_station)
modern_home_station = ModerHomeStation(weather_data=weather_station)

print("\nnew weather date")
weather_station.set_measurements(15, 60, 1005)
print("\nnew weather date")
weather_station.set_measurements(-5, 50, 999)
print("\nnew weather date")
weather_station.set_measurements(0, 75, 1020)
print("\nnew weather date")
weather_station.set_measurements(35, 60, 1098)
